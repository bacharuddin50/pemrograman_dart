import 'dart:io';

import 'package:switch_case_02/switch_case_02.dart' as switch_case_02;

void main(List<String> arguments) {
  print("=====TANGGAL=====");
  stdout.write("Input hari: ");
  var hari = stdin.readLineSync()!;
  stdout.write("Input bulan: ");
  var bulan = stdin.readLineSync()!;
  stdout.write("Input tahun: ");
  var tahun = stdin.readLineSync()!;

  switch (hari) {
    case "1":
      {
        print("1");
        break;
      }
    case "5":
      {
        print("5");
        break;
      }
    case "10":
      {
        print("10");
        break;
      }
    case "21":
      {
        print("21");
        break;
      }
  }
  switch (bulan) {
    case "1":
      {
        print("Januari");
        break;
      }
    case "2":
      {
        print("Februari");
        break;
      }
    case "3":
      {
        print("Maret");
        break;
      }
    case "4":
      {
        print("April");
        break;
      }
    case "5":
      {
        print("Mei");
        break;
      }
    case "6":
      {
        print("Juni");
        break;
      }
    case "7":
      {
        print("Juli");
        break;
      }
    case "8":
      {
        print("Agustus");
        break;
      }
    case "9":
      {
        print("September");
        break;
      }
    case "10":
      {
        print("Oktober");
        break;
      }
    case "11":
      {
        print("November");
        break;
      }
    case "12":
      {
        print("Desember");
        break;
      }
  }
  switch (tahun) {
    case "1920":
      {
        print("1920");
        break;
      }
    case "1945":
      {
        print("1945");
        break;
      }
    case "1990":
      {
        print("1990");
        break;
      }
  }
}
