import 'package:ternary_operator/ternary_operator.dart' as ternary_operator;
import 'dart:io';

void main() {
  print("=====DART=====");
  stdout.write("Option: ");
  var option = stdin.readLineSync()!;

  if (option == "y") {
    print("anda akan menginstall aplikas dart");
  } else if (option == "n") {
    print("aborted");
  } else {
    print("===ERROR===");
  }
}
