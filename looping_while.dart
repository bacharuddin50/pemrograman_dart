import 'package:looping_while/looping_while.dart' as looping_while;

void main(List<String> arguments) {
  var deretA = 10;
  var jumlahA = 0;
  print("LOOPING PERTAMA");
  while (deretA > 0) {
    jumlahA += 2;
    deretA--;
    print(jumlahA.toString() + " - I love coding");
  }
  print("LOOPING KEDUA");
  var deretB = 10;
  var jumlahB = 22;
  while (deretB > 0) {
    jumlahB -= 2;
    deretB--;
    print(jumlahB.toString() + " - I will become a mobile developer");
  }
}
